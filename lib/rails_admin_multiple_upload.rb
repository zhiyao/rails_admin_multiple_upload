require "simple_form"
require "rails_admin_multiple_upload/engine"
require 'rails_admin/config/actions/base'

module RailsAdminMultipleUpload
end

require 'rails_admin/config/actions'

module RailsAdmin
  module Config
    module Actions
      class MultipleUpload < Base
        RailsAdmin::Config::Actions.register(self)
        register_instance_option :member do
          true
        end

        register_instance_option :link_icon do
          'icon-check'
        end

        register_instance_option :http_methods do
          [:get, :post]
        end

        register_instance_option :controller do

          Proc.new do
            if request.post?
              @album = Album.find(params[:album_id])
              puts params
              if @album.update_attributes(params.require(:album).permit(photos_attributes: [:id, :caption, :file, :album_id]))
                render json: [@album.photos.order('id desc').first]
              else
                flash[:error] = 'Error uploading images. Please try again'
                render :action => @action.template_name
              end
            elsif request.get?
              album_id = params[:album_id]
              album_id ||= params[:id]
              @album = Album.find(album_id)
              render :action => @action.template_name
            end

          end
        end
      end
    end
  end
end
